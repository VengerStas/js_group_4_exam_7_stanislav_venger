import React, { Component } from 'react';
import Items from './components/Items/Items';
import Order from './components/Order/Order';
import './App.css';

class App extends Component {
    state = {
        menu: [
            {name: 'Hamburger', price: 80, count: 0},
            {name: 'Cheeseburger', price: 90, count: 0},
            {name: 'Fries', price: 45, count: 0},
            {name: 'Coffee', price: 70, count: 0},
            {name: 'Tea', price: 50, count: 0},
            {name: 'Cola', price: 40, count: 0},
        ],

        totalPrice: 0
    };

    disabledText = ['Block'];

    addItems = fill => {
        this.disabledText = ['None'];
      const menu = [...this.state.menu];
      let totalPrice = this.state.totalPrice;
      for (let i = 0; i < menu.length; i++){
          if (menu[i].name === fill.name) {
              menu[i].count++;
              totalPrice += menu[i].price;
          }
      }
      this.setState({menu, totalPrice})
    };

    deleteItems = fill => {
        const menu = [...this.state.menu];
        let totalPrice = this.state.totalPrice;
        for (let i = 0; i < menu.length; i++){
            if (menu[i].name === fill.name && menu[i].count !== 0) {
                menu[i].count--;
                totalPrice -= menu[i].price;
            }
        }
        this.setState({menu, totalPrice})
    };

  render() {
    return (
      <div className="App">

          <Order
            menu={this.state.menu}
            count={this.state.menu}

            deleteHandler={(item) => this.deleteItems(item)}
            totalPrice={this.state.totalPrice}
          >
              <p className={this.disabledText}>Order is empty</p>
              <p className={this.disabledText}>Please add some items!</p>
          </Order>

          <Items
            addHandler={(name) => this.addItems(name)}
          />
      </div>
    );
  }
}

export default App;
