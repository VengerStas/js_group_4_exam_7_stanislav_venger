import React from 'react';
import './Item.css';


const Item = props => {
    const getItem = () => {
        const items = [];
        for (let i = 0; i < props.count; i++){
            items.splice(0,1,props.count)
        }
        console.log(items);
        return items;
    };

    return (
        getItem().map((name, index) => {
            return (
                <div key={index} className="menu-item">
                    <span className="menu-item-name">{props.name}: </span>
                    <span className="menu-item-count">X{props.count}</span>
                    {props.children}
                </div>
            )
        })
    )
};

export default Item;