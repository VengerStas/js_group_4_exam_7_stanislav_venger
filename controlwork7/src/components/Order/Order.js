import React from 'react';
import Item from '../Item/Item';
import './Order.css';
import deleteImg from "../../assets/delete.jpg";

const Order = props => {
    return (
        <div className="order">
            <h4 className="order-title">Order details:</h4>
            {props.children}
            {
                props.menu.map((item, index) => {
                    return (
                        <Item
                            key={index}
                            name={item.name}
                            count={item.count}
                        >
                            <button className="delete-btn" onClick={() => props.deleteHandler(item)}>
                                <img className="menu-item-delete" src={deleteImg} alt="delete"/>
                            </button>
                        </Item>
                    )
                })
            }
            <p className="total-price">Total price: {props.totalPrice} KGS</p>
        </div>
    )
};

export default Order;
