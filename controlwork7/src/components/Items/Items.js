import React from 'react';
import './Items.css';
import hamburgImg from '../../assets/hamburger.jpg';
import cheeseburgerImg from '../../assets/Cheeseburger.png';
import friesImg from '../../assets/fries.png';
import coffeeImg from '../../assets/Coffee.png';
import teaImg from '../../assets/tea.jpg';
import colaImg from '../../assets/cocacola.png';

const Items = props => {
    const Items = [
        {name: 'Hamburger', price: 80, image: hamburgImg},
        {name: 'Cheeseburger', price: 90, image: cheeseburgerImg},
        {name: 'Fries', price: 45, image: friesImg},
        {name: 'Coffee', price: 70, image: coffeeImg},
        {name: 'Tea', price: 50, image: teaImg},
        {name: 'Cola', price: 40, image: colaImg}
    ];

    return (
        <div className="menu">
            <h4 className="menu-title">Add items:</h4>
            {
                Items.map((name, index) => {
                    return (
                        <button className="item" key={index} onClick={() => props.addHandler(name)}>
                           <img src={name.image} alt={name.name}/>
                            <p>{name.name}</p>
                            <p>Price: {name.price} KGS</p>
                        </button>
                    )
                })
            }
        </div>
    )
};

export default Items;